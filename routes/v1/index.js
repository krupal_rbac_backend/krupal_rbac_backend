var bCrypt = require('bcrypt-nodejs');
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code
/* congig */
var configuration = require("../../config");
var Roles = require("../../models/roles");
/* utils*/

/* handler */
var UserHandler = require("../../handlers/user_handler");
var RoleHandler = require("../../handlers/roles_handler");
var PermissionHandler = require("../../handlers/permissions_handler");
var FunctionsHandler = require("../../handlers/functions_handler");

/* Generates hash using bCrypt*/
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

/* Authentication function*/
var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    }
    // if the user is not authenticated then redirect him to the login page
    res.redirect(BASE_API_URL + '/');
};

var isAuthenticatedAccessToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    //console.log(token);
    //var token = req.headers['x-access-token'];
    // decode token
    if (token) {
        // console.log(token);
        // verifies secret and checks exp
        jwt.verify(token, configuration.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other
                //console.log(decoded)
                req.user = decoded;
                next();
            }
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            statuscode: 203,
            msgkey: "api.access.token.failed",
            v: version
        });
    }
}

var rbac = function (function_id, permission_name) {
    return (req, res, next) => {
        var role_name = req.body.role_name || req.query.role_name || req.headers.role_name;
        // var functions_id = req.body.functions_id || req.query.functions_id || req.headers.functions_id;
        // var operation_name = req.body.operation || req.query.operation || req.headers.operation;
        var functions_id = function_id;
        var operation_name = permission_name;
        if (role_name && functions_id && operation_name) {
            console.log("role_name: ", role_name);
            console.log("functions_id: ", functions_id);
            console.log("operation_name: ", operation_name);
            Roles.findOne({
                "role_name": role_name,
                "function_permission.functions_id": functions_id
            }, function (err, role) {
                if (role == null) {
                    return res.json({
                        success: false,
                        message: 'you do not have permission to access.'
                    });
                } else {
                    //console.log(role.function_permission);
                    var my_json = role.function_permission;
                    var data = [];
                    data.push(my_json[functions_id - 1]);
                    //console.log("data", data);
                    console.log(data[0].list);
                    console.log(data[0].create);
                    console.log(data[0].update);
                    console.log(data[0].delete);
                    console.log(data[0].view);
                    if (data[0].create == operation_name || data[0].list == operation_name || data[0].update == operation_name || data[0].delete == operation_name || data[0].view == operation_name) {
                        next();
                    } else {
                        return res.json({
                            success: false,
                            message: 'you do not have permission to access..'
                        });
                    }
                }
            });

        } else {
            return res.status(403).send({
                statuscode: 203,
                msgkey: "you do not have permission to access",
                v: version
            });
        }
    }
}

module.exports = function (passport) {

    /*API to accress root*/
    router.get(BASE_API_URL + '/', function (req, res) {

        var reponseJson = {
            statuscode: 200,
            msgkey: "login.first.to.access.api",
            v: version
        };
        res.json(reponseJson);
    });

    /* sign up user exist*/
    router.get(BASE_API_URL + '/_signupfailure', function (req, res) {

        var reponseJson = {
            statuscode: 203,
            msgkey: "auth.signup.exist",
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Login*/
    router.post(BASE_API_URL + '/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else {
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "login success",
                    v: version,
                    data: user
                };
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for GET Home Page */
    router.get(BASE_API_URL + '/home', function (req, res) {
        if (req.user.username) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "login.success",
                v: version,
                data: req.user
            };
        }
        res.json(reponseJson);
    });

    /* API for login failure*/
    router.get(BASE_API_URL + '/_loginfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "login.failure",
            registered: false,
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Handle Logout */
    router.get(BASE_API_URL + '/logout', function (req, res) {
        req.session.destroy(function (err) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "logout.success",
                v: version
            };
            res.json(reponseJson);
        });
    });

    /* API for Handle Registration POST */
    router.post('/signup', function (req, res, next) {
        passport.authenticate('signup', function (err, user, info) {
            if (err) {
                //  console.log("Error 1: ", err);
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "auth.signup.exist",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
            } else {
                var registered = false;

                var reponseJson = {
                    statuscode: 200,
                    msgkey: "auth.signup.success",
                    username: user.username,
                    v: version
                };
                req.logout();
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for signup success page */
    router.get(BASE_API_URL + '/_signupsuccess', function (req, res) {
        var registered = false;
        if (req.user.isActive === "1") {
            registered = true;
        }
        var reponseJson = {
            statuscode: 200,
            msgkey: "auth.signup.success",
            registered: registered,
            username: req.user.username,
            v: version
        };

        // res.render('home', { user: req.user });
        req.logout();
        res.json(reponseJson);
    });

    //API for add Role
    router.post(BASE_API_URL + '/add_role', function (req, res) {
        var data = req.body;
        RoleHandler.add_role(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for get Role
    router.get(BASE_API_URL + '/get_role', function (req, res) {
        var data = req.query;
        RoleHandler.get_role(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for list Roles
    router.get(BASE_API_URL + '/list_roles', function (req, res) {
        RoleHandler.list_roles(function (response) {
            response.version = version;
            res.json(response);
        });
    });

     //API for delete
     router.post(BASE_API_URL + '/delete_role', function (req, res) {
        var data = req.body;
        RoleHandler.delete_role(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for add permission
    router.post(BASE_API_URL + '/add_permission', function (req, res) {
        var data = req.body;
        PermissionHandler.add_permission(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for get permission
    router.get(BASE_API_URL + '/get_permission', function (req, res) {
        var data = req.query;
        PermissionHandler.get_permission(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for list permissions
    router.get(BASE_API_URL + '/list_permissions', function (req, res) {
        PermissionHandler.list_permissions(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for add Function
    router.post(BASE_API_URL + '/add_function', function (req, res) {
        var data = req.body;
        FunctionsHandler.add_function(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for get Function
    router.get(BASE_API_URL + '/get_function', function (req, res) {
        var data = req.query;
        FunctionsHandler.get_function(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    //API for list Functions
    router.get(BASE_API_URL + '/list_functions', function (req, res) {
        FunctionsHandler.list_functions(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/add_user', rbac("1", "create"), function (req, res) {
        var data = req.body;
        UserHandler.add_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/edit_user', rbac("1", "update"), function (req, res) {
        var data = req.body;
        UserHandler.edit_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_user', rbac("1", "view"), function (req, res) {
        var data = req.query;
        UserHandler.get_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_users', rbac("1", "list"), function (req, res) {
        UserHandler.list_users(function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/delete_user', rbac("1", "delete"), function (req, res) {
        var data = req.body;
        UserHandler.delete_user(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    return router;
};
