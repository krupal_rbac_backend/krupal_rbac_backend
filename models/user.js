var mongoose = require('mongoose');
module.exports = mongoose.model('user', {
    user_id : String,
    user_role : String,
    password : String,
    username : String,
    email : String,
    mobile : Number,
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: Date,
    is_active : {type: Boolean, default: true}
});
