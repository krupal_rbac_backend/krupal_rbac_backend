var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var bCrypt = require('bcrypt-nodejs');
var configuration = require("../config");
//var email = require("../utils/mailer");
var uuidV4 = require("../utils/utils");

module.exports = function (passport) {
    passport.use('signup', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    }, function (req, username, password, done) {

        findOrCreateUser = function () {
            //console.log("inside signup: ", req);
            var data = req.body;
            User.findOne({ username: data.username }, function (err, user) {
                if (user) {
                    console.log("Username " + username);
                    return done({ message: "Username " + username }, false, { message: req.flash(username + " is already taken.") });
                }
                //User.findOne({ "mobile": data.mobile}, function (err, user1) {});
                User.findOne({ "email": data.email}, function (err, user1) {
                    if (err) {
                        return done({ message: err });
                    }
                    if (user1) {
                        console.log("email " + data.email);
                        return done({ message: "Username " + data.email }, false, { message: req.flash(data.email + " is already taken.") });
                    } else {
                        var newUser = new User();
                        newUser.username = username;
                        newUser.password = createHash(password);
                        var date = new Date();
                        newUser.createdAt = date.toUTCString();
                        newUser.expiresAt = date.toUTCString();
                        createHash(username);
                        var time = new Date().getTime();
                        var crypto = require('crypto');
                        var name = username;
                        var signupcry = crypto.createHash('md5').update(name).digest('hex');
                        var passtoken = signupcry.concat(time);
                        var unique_id = uuidV4.getUniqueId();
                        newUser.user_id = unique_id;
                        newUser.passtoken = passtoken;
                        newUser.user_role = data.user_role;
                        newUser.mobile = data.mobile;
                        newUser.email = data.email;
                        newUser.is_active = true;
                        newUser.email_verified = false;
                        newUser.save(function (err) {
                            if (err) {
                                console.log('Error in Saving user: ' + err);
                                return done(err);
                            }
                            console.log("User Registration succesful");
                            var msgBody = "Hello,<br> Please Click on the link to verify your email.<br><a href=" + configuration.SIGNUP_PASSWORD_URL + passtoken + " >Click here to verify</a>..";
                            let mailOptions = {
                                from: '"Krupal-Node-Test" <' + configuration.SENDER_EMAIL + '>', // sender address
                                to: data.email, // list of receivers
                                subject: 'Welcome to Krupal-Node-Test', // Subject line
                                text: "req.body.body", // plain text body
                                html: msgBody
                            };
                            email.sendMailNotification(mailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                }
                                console.log('Message %s sent: %s', info.messageId, info.response);
                                res.render('index');
                            });
                            return done(null, newUser);
                        });
                    }
                });
            });
        };
        process.nextTick(findOrCreateUser);
    })
    );
    /* Generates hash using bCrypt*/
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}
