var User = require("../models/user");
var bCrypt = require('bcrypt-nodejs');
var uuidV4 = require("../utils/utils");

module.exports = {

    //Add User
    add_user: function (data, callback) {
        UserData = function () {
            User.findOne({
                "username": data.username
            }, function (err, user_data) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding user_id database error"
                    });
                } else if (user_data != null) {
                    callback({
                        statuscode: 304,
                        msg: "User already exist"
                    });

                } else {
                    var add_user = new User();
                    add_user.user_id = uuidV4.getUniqueId();;
                    add_user.password = createHash(data.password);
                    add_user.user_role = data.user_role;
                    add_user.username = data.username;
                    add_user.email = data.email;
                    add_user.mobile = data.mobile;
                    add_user.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "Saving user, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "User added successfully",
                                data: add_user
                            });
                        }
                    });
                }
            });
        }
        process.nextTick(UserData);
        /* Generates hash using bCrypt*/
        var createHash = function (password) {
            return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
        }
    },

    //View User
    get_user: function (data, callback) {
        UserData = function () {
            User.findOne({
                "user_id": data.user_id
            }, function (err, user_details) {
                if (err) {
                    callback({
                        msg: "Finding user_id from database , an error",
                        statuscode: 500,
                    });
                } else if (user_details == null) {
                    callback({
                        msg: "user_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "User details",
                    statuscode: 200,
                    data: user_details
                });
            });
        }
        process.nextTick(UserData);
    },

    //List all users
    list_users: function (callback) {
        userData = function () {
            User.find({}, function (err, users_list) {
                if (err) {
                    callback({
                        msg: "Finding user from database , an error",
                        statuscode: 500,
                    });
                } else if (users_list == null) {
                    callback({
                        msg: "No user found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of users",
                    statuscode: 200,
                    data: users_list
                });
            });
        }
        process.nextTick(userData);
    },

    //Edit user
    edit_user: function (data, callback) {
        update_user_data = function () {
            if (data.user_id == undefined || data.user_id == '') {
                callback({
                    msgkey: "User not found",
                    statuscode: 404,
                });
            } else {
                User.findOne({
                    "user_id": data.user_id,
                }, function (err, user_data) {
                    var newdata = user_data;
                    if (err) {
                        callback({
                            statuscode: 500,
                            msg: "Finding user_id database error "
                        });
                    } else if (user_data == null) {
                        callback({
                            statuscode: 404,
                            msg: "user_id not found "
                        });
                    } else {
                        if (data.email !== undefined && data.email !== "") {
                            newdata.email = data.email;
                        }
                        if (data.mobile !== undefined && data.mobile !== "") {
                            newdata.mobile = data.mobile;
                        }
                        newdata.save(function (err, userupdated) {
                            callback({
                                statuscode: 404,
                                msg: "User updated Successfully ",
                                data: userupdated
                            });
                        });
                    }
                });
            }
        }
        process.nextTick(update_user_data);
    },

    //Delete user
    delete_user: function (data, callback) {
        delete_user_data = function () {
            if (data.user_id == undefined || data.user_id == '') {
                callback({
                    msgkey: "User not found",
                    statuscode: 404,
                });
            } else {
                User.deleteOne({
                    "user_id": data.user_id
                }, function (err, user) {
                    if (err) {
                        callback({
                            msg: "Finding user_id from database , an error",
                            statuscode: 500,
                        });
                    } else if (user == null) {
                        callback({
                            msg: "user_id does not exist",
                            statuscode: 404,
                        });
                    }
                    callback({
                        msg: "User deleted successfully",
                        statuscode: 200,
                    });
                });
            }
        }
        process.nextTick(delete_user_data);
    }
}
