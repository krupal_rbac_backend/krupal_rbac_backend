var functions = require("../models/functions");

module.exports = {

    //Add Function
    add_function: function (data, callback) {
        add_functions_data = function () {
            functions.findOne({
                "functions_id": data.functions_id
            }, function (err, functionsdata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding functions_id database error"
                    });
                } else if (functionsdata != null) {
                    callback({
                        statuscode: 304,
                        msg: "functions already exist"
                    });

                } else {
                    var addfunctions = new functions();
                    addfunctions.functions_id = data.functions_id;
                    addfunctions.functions_name = data.functions_name;
                    addfunctions.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving functions, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "functions added successfully",
                                data: result
                            });
                        }
                    });
                }
            });
        };
        process.nextTick(add_functions_data);
    },

    //Get Function
    get_function: function (data, callback) {
        get_function_data = function () {
            functions.findOne({
                "functions_id": data.functions_id
            }, function (err, functions) {
                if (err) {
                    callback({
                        msg: "Finding functions_id from database , an error",
                        statuscode: 500,
                    });
                } else if (functions == null) {
                    callback({
                        msg: "functions_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "function details",
                    statuscode: 200,
                    data: functions
                });
            });
        };
        process.nextTick(get_function_data);
    },

    //List functions
    list_functions: function (callback) {
        list_functions_data = function () {
            functions.find({}, function (err, functions) {
                if (err) {
                    callback({
                        msg: "Finding functions from database , an error",
                        statuscode: 500,
                    });
                } else if (functions == null) {
                    callback({
                        msg: "No functions found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of functions",
                    statuscode: 200,
                    data: functions
                });
            });
        };
        process.nextTick(list_functions_data);
    }
};
