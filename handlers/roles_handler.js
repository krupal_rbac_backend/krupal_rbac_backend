var Roles = require("../models/roles");

module.exports = {

    //Add Role
    add_role: function (data, callback) {
        add_role_data = function () {
            Roles.findOne({
                "role_id": data.role_id
            }, function (err, roledata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding event_id database error"
                    });
                } else if (roledata != null) {
                    callback({
                        statuscode: 304,
                        msg: "Role already exist"
                    });

                } else {
                    var addrole = new Roles();
                    addrole.role_id = data.role_id;
                    addrole.role_name = data.role_name;
                    addrole.function_permission = data.function_permission;
                    addrole.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving Role, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "Role added successfully",
                                data: result
                            });
                        }
                    });
                }
            });
        };
        process.nextTick(add_role_data);
    },

    //Get Role
    get_role: function (data, callback) {
        get_role_data = function () {
            Roles.findOne({
                "role_name": data.role_name
            }, function (err, role) {
                if (err) {
                    callback({
                        msg: "Finding role_id from database , an error",
                        statuscode: 500,
                    });
                } else if (role == null) {
                    callback({
                        msg: "role_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "Role details",
                    statuscode: 200,
                    data: role
                });
            });
        };
        process.nextTick(get_role_data);
    },

    //List Roles
    list_roles: function (callback) {
        list_roles_data = function () {
            Roles.find({}, function (err, roles) {
                if (err) {
                    callback({
                        msg: "Finding roles from database , an error",
                        statuscode: 500,
                    });
                } else if (roles == null) {
                    callback({
                        msg: "No roles found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of roles",
                    statuscode: 200,
                    data: roles
                });
            });
        };
        process.nextTick(list_roles_data);
    },

    //Delete Role
    delete_role: function (data, callback) {
        delete_role_data = function () {
            if (data.role_id == undefined || data.role_id == '') {
                callback({
                    msgkey: "Role not found",
                    statuscode: 404,
                });
            } else {
                Roles.deleteOne({
                    "role_id": data.role_id
                }, function (err, role) {
                    if (err) {
                        callback({
                            msg: "Finding role_id from database , an error",
                            statuscode: 500,
                        });
                    } else if (role == null) {
                        callback({
                            msg: "role_id does not exist",
                            statuscode: 404,
                        });
                    }
                    callback({
                        msg: "Role deleted successfully",
                        statuscode: 200,
                    });
                });
            }
        }
        process.nextTick(delete_role_data);
    }
};
