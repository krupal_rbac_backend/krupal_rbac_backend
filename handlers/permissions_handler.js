var Permission = require("../models/permissions");

module.exports = {

    //Add Permission
    add_permission: function (data, callback) {
        add_permission_data = function () {
            Permission.findOne({
                "permission_id": data.permission_id
            }, function (err, permissiondata) {
                if (err) {
                    callback({
                        statuscode: 500,
                        error: err,
                        msg: "finding permission_id database error"
                    });
                } else if (permissiondata != null) {
                    callback({
                        statuscode: 304,
                        msg: "permission already exist"
                    });

                } else {
                    var addpermission = new Permission();
                    addpermission.permission_id = data.permission_id;
                    addpermission.permission_name = data.permission_name;
                    addpermission.save(function (err, result) {
                        if (err) {
                            callback({
                                statuscode: 500,
                                error: err,
                                msg: "saving Permission, database error"
                            });
                        } else {
                            callback({
                                statuscode: 200,
                                msg: "Permission added successfully",
                                data: result
                            });
                        }
                    });
                }
            });
        };
        process.nextTick(add_permission_data);
    },

    //Get Permission
    get_permission: function (data, callback) {
        get_permission_data = function () {
            Permission.findOne({
                "permission_id": data.permission_id
            }, function (err, permission) {
                if (err) {
                    callback({
                        msg: "Finding permission_id from database , an error",
                        statuscode: 500,
                    });
                } else if (permission == null) {
                    callback({
                        msg: "permission_id does not exist",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "permission details",
                    statuscode: 200,
                    data: permission
                });
            });
        };
        process.nextTick(get_permission_data);
    },

    //List Permissions
    list_permissions: function (callback) {
        list_permissions_data = function () {
            Permission.find({}, function (err, permissions) {
                if (err) {
                    callback({
                        msg: "Finding permission from database , an error",
                        statuscode: 500,
                    });
                } else if (permissions == null) {
                    callback({
                        msg: "No permissions found",
                        statuscode: 404,
                    });
                }
                callback({
                    msg: "List of permissions",
                    statuscode: 200,
                    data: permissions
                });
            });
        };
        process.nextTick(list_permissions_data);
    }
};
